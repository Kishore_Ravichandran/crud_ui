import { Component, OnInit } from '@angular/core';
import {Validators,FormControl,FormGroup,FormBuilder, FormArray} from '@angular/forms';
import {ElementRef} from '@angular/core';
import {ViewChild} from '@angular/core';
import {SelectItem} from 'primeng/api';
import {MessageService} from 'primeng/api';
import { User } from '../model/user';
import { DatePipe } from '@angular/common';
import { CrudService } from '../crud.service';


@Component({
  selector: 'app-crud',
  templateUrl: './crud.component.html',
  styleUrls: ['./crud.component.css'],
  providers : [MessageService]
})
export class CrudComponent implements OnInit {

  @ViewChild('file') file: ElementRef;

  userform: FormGroup;

  fileToUpload = [];

  userDetails : User[] = [];


  position: SelectItem[];

  chennai:boolean = false;
  blore: boolean = false;
  pune: boolean = false;

  isEmail:boolean = false;

  isView: boolean = false;
  isAdd : boolean = true;

    submitted: boolean;


    selectedType: string;
    public imagePath;
  imgURL: any;

  constructor(private fb: FormBuilder, private messageService: MessageService,private datePipe : DatePipe,private crudService : CrudService) { }

  ngOnInit(): void {

    // Form builder

    this.userform = this.fb.group({
      firstname: ['', [Validators.required]],
      file:null,
      countrycode: ['', [Validators.required]],
      mobnumber : ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
      email : ['', [Validators.required]],
      positionType : ['',Validators.required],
      date : ['',Validators.required],
      fileSource: new FormControl('', [Validators.required]),
      checkList : this.fb.group({
        cb1: [false, Validators.required],
        cb2: [false, Validators.required],
        cb3: [false, Validators.required],
      })

  });

  // Select box for Position

  this.position = [
    {label : 'FT',value:'Full Time'},
    {label : 'PT' , value : 'Part Time'},
    {label : 'Consultant',value:'Consultant'}
];

this.getUserList(); // retrieve User List

  }

  getUserList() { // Get the user list from server
    this.crudService.getUser().subscribe(user => {
      if(user) {
        console.log('User Details' + user);
        this.userDetails = [...user];
        console.log('User ' + JSON.stringify(this.userDetails));
      }
    })
  }


// Setting the values from Form
setUser(email: String): void {
  this.blore= false;
  this.chennai = false;
  this.pune = false;
  let user = this.userDetails.find(user => user.email === email);
  

  // Populate the user form
  this.userform.get('firstname').setValue(user.name);
  this.userform.get('countrycode').setValue(user.countrycode);
  this.userform.get('mobnumber').setValue(user.mobile);
  this.userform.get('date').setValue(user.doj);
  this.userform.get('positionType').setValue(user.job);
  this.userform.get('email').setValue(user.email);
  let location = user.location.split(',');
  
  if(location.includes('Bangalore')) {
    this.blore = true;
  }

  if(location.includes('Chennai')) {
    this.chennai = true;
  }

  if(location.includes('Pune')) {
    this.pune = true;
  }

}

// Get the user details from the form fields

get user(): User {
  var checkList = [];
  // const formData = new FormData();
  // formData.append("image", this.fileToUpload[0].file, this.fileToUpload[0].file.name);
  const userVal = this.userform.value;
  let doj = userVal.date;
  if(userVal.date instanceof Date){
    doj = this.datePipe.transform(userVal.date, 'dd/MM/yyyy');
  }
  
  if(this.chennai){checkList.push('Chennai')}
  if(this.blore){checkList.push('Bangalore')}
  if(this.pune){checkList.push('Pune')}

  
  let location = checkList.join();

  // console.log(formData.get('image'));
  
  let user = new User(userVal.firstname, userVal.email, userVal.mobnumber, doj,userVal.positionType,location,userVal.countrycode,userVal.file);
  return user;

  
}

// Delete an user using mailid

delete(email: string) {
  this.crudService.delete(email).subscribe(result => {
    if(result) {
      this.userDetails = this.userDetails.filter(user => user.email !== email);
    }
   
  },error => {
    console.log('Error while deleting' + error);
  })
}

//On click of edit button

edit(email:string) {
  this.setUser(email);
  this.isAdd = false;
  this.isEmail = true;
}

// On submitting the form,either add/update values

onSubmit() : void{
  if(this.userform.value.email) {
    this.userDetails = this.userDetails.filter(user => user.email !== this.userform.value.email)
  }

  const user = this.user;

  if(!this.isAdd) {
    this.crudService.update(user).subscribe(result => {  // Update values in the form
      if(result) {
        this.userDetails.push(user);
      }
    },error => {
      console.log('Error while updating User',error);
    })
    this.isAdd = true;
    this.isEmail = false;
  } else {
    this.crudService.save(user).subscribe(result => {  // Add values to the form
      if(result.status === 200) {
        console.log('User' + JSON.stringify(result));
        this.userDetails.push(user);
      }
    },error => {
      console.log('Error while adding the User', error);
    })
  }
  this.userform.reset();
}

preview(files) {
  if (files.length === 0)
    return;

  var mimeType = files[0].type;
  if (mimeType.match(/image\/*/) == null) {
    return;
  }


  var reader = new FileReader();
  this.imagePath = files;
  reader.readAsDataURL(files[0]); 
  reader.onload = () => { 
  this.imgURL = reader.result;

  //const file = files[0];
}

}
}


