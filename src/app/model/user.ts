export class User {
    constructor(
        public name: string='',
        public email: string = '',
        public mobile: string = '',
        public doj: string = '',
        public job: string = '',
        public location: string = '',
        public countrycode: string = '',
        public image: any
    ) { }
}