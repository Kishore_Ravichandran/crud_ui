import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import { Observable } from 'rxjs';
import { User } from './model/user';

@Injectable({
  providedIn: 'root'
})
export class CrudService {

  constructor(private http: HttpClient) { }

  getUser(): Observable<any> {  // retrieve user list
    return this.http.get('/api/vi/user/list')
  }

  save(user : User): Observable<any> {  // add user
    return this.http.post('/api/vi/user/add',user,{ observe: 'response' });
  }

  update(user: User): Observable<any>  // update user based on mail id
  {
    return this.http.post('/api/vi/user/update',user,{ observe: 'response' });
  }

  delete(email: string): Observable<any> { // delete user based on mailid
    return this.http.get("/api/vi/user/delete/" + email);
  }
}
